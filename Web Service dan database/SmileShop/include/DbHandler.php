<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /**
    * Fetching semua users
    */
    public function getAllUser(){
        $stmt = $this->conn->prepare("SELECT id_user, username, password, is_admin FROM users ORDER BY id_user ASC");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();

        return $tasks;
    }

    /**
    * Fetching semua users per username
    */
    public function getUserPerUsername($username) {
        $stmt = $this->conn->prepare("SELECT id_user, username, password, is_admin FROM users WHERE username = ? ORDER BY id_user ASC");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    /**
     * Insert new users
     */
    public function insertUser($username, $password, $is_admin) {
        $tasks = $this->getUserPerUsername($username);
        $tasks = $tasks->fetch_assoc();
        if ($tasks["username"] == $username) {
            return "0";
        } else {
            $stmt = $this->conn->prepare("INSERT INTO users (username, password, is_admin) VALUES (?, ?, ?)");
            $stmt->bind_param("ssi", $username, $password, $is_admin);
            $stmt->execute();
            $stmt->close();
            return "1";
        }
    }


    /**
     * Fetching semua produks
     */
    public function getAllProduk(){
        $stmt = $this->conn->prepare("SELECT id_produk, nama_produk, harga_produk FROM produks ORDER BY id_produk ASC");
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();

        return $tasks;
    }

    /**
     * Fetching semua produks per nama produk
     */
    public function getProdukPerNamaProduk($nama_produk) {
        $stmt = $this->conn->prepare("SELECT id_produk, nama_produk, harga_produk FROM produks WHERE nama_produk = ? ORDER BY id_produk ASC");
        $stmt->bind_param("s", $nama_produk);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

//  public function verifyRequiredParams($required_fields) {
//        $error = false;
//        $error_fields = "";
//        $request_params = array();
//        $request_params = $_REQUEST;
//        // Handling PUT request params
//        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
//            $app = \Slim\Slim::getInstance();
//            parse_str($app->request()->getBody(), $request_params);
//        }
//        foreach ($required_fields as $field) {
//            if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
//                $error = true;
//                $error_fields .= $field . ', ';
//            }
//        }
//        if ($error) {
//            // Required field(s) are missing or empty
//            // echo error json and stop the app
//            $response = array();
//            $app = \Slim\Slim::getInstance();
//            $response["error"] = true;
//            $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
//            echoRespnse(400, $response);
//            $app->stop();
//        }
//  }
}

?>
