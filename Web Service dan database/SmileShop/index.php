<?php

require_once 'include/DbHandler.php';
require_once 'include/PassHash.php';
require 'libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */

//users
$app->post('/allUser', function () {
	$response = array();

	$db = new DbHandler();

	// fetching all user
	$result = $db->getAllUser();
	//print_r($result);

	$response["error"] = false;
	$response["user"] = array();

	// looping through result and preparing materi array
	while ($strData = $result->fetch_assoc()) {
	    $tmp = array();
	    $tmp["id_user"] = utf8_encode($strData["id_user"]);
	    $tmp["username"] = utf8_encode($strData["username"]);
	    $tmp["password"] = utf8_encode($strData["password"]);
	    $tmp["is_admin"] = utf8_encode($strData["is_admin"]);

	    array_push($response["user"], $tmp);
	}

	echoRespnse(200, $response);
});

$app->post('/userPerUsername', function() use ($app) {
	// check for required params
	//	verifyRequiredParams(array('username'));
	$response = array();
	// reading post params
	$username = $app->request->post('username');

	$db = new DbHandler();

	// fetching all user per username
	$result = $db->getUserPerUsername($username);
	$response["error"] = false;
	$response["user"] = array();

	// looping through result and preparing materi array
	while ($strData = $result->fetch_assoc()) {
		$tmp = array();
		$tmp["id_user"] = utf8_encode($strData["id_user"]);
		$tmp["username"] = utf8_encode($strData["username"]);
		$tmp["password"] = utf8_encode($strData["password"]);
		$tmp["is_admin"] = utf8_encode($strData["is_admin"]);

		array_push($response["user"], $tmp);
	}
	echoRespnse(200, $response);
});

$app->post('/insertUser', function() use ($app) {
	// check for required params
	//	verifyRequiredParams(array('username'));
	$response = array();
	// reading post params
	$username = $app->request->post('username');
	$password = $app->request->post('password');
	$is_admin = $app->request->post('is_admin');

	$db = new DbHandler();

	// We have to check if the username is present before we insert it
	$result = $db->insertUser($username, $password, $is_admin);
	$response["error"] = false;
	$response["is_work"] = $result;
	if ($result == 1) {
		$response["message"] = "Registered successfully";
	} else {
		$response["message"] = "This user is already exists";
	}
	echoRespnse(200, $response);
});

//produks
$app->post('/allProduk', function () {
	$response = array();

	$db = new DbHandler();

	// fetching all produk
	$result = $db->getAllProduk();
	//print_r($result);


	$response["error"] = false;
	$response["produk"] = array();

	// looping through result and preparing materi array
	while ($strData = $result->fetch_assoc()) {
	    $tmp = array();
	    $tmp["id_produk"] = utf8_encode($strData["id_produk"]);
	    $tmp["nama_produk"] = utf8_encode($strData["nama_produk"]);
	    $tmp["harga_produk"] = utf8_encode($strData["harga_produk"]);

	    array_push($response["produk"], $tmp);
	}

	echoRespnse(200, $response);
});

$app->post('/produkPerNamaProduk', function() use ($app) {
	// check for required params
	//	verifyRequiredParams(array('namaProduk'));
	$response = array();
	// reading post params
	$nama_produk = $app->request->post('nama_produk');

	$db = new DbHandler();

	// fetching all produk per nama
	$result = $db->getBukuPerPengarang($nama_produk);
	$response["error"] = false;
	$response["produk"] = array();

	// looping through result and preparing materi array
	while ($strData = $result->fetch_assoc()) {
		$tmp = array();
		$tmp["id_produk"] = utf8_encode($strData["id_produk"]);
		$tmp["nama_produk"] = utf8_encode($strData["nama_produk"]);
		$tmp["harga_produk"] = utf8_encode($strData["harga_produk"]);

		array_push($response["produk"], $tmp);
	}
	echoRespnse(200, $response);
});

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 * Daftar response
 * 200	OK
 * 201	Created
 * 304	Not Modified
 * 400	Bad Request
 * 401	Unauthorized
 * 403	Forbidden
 * 404	Not Found
 * 422	Unprocessable Entity
 * 500	Internal Server Error
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

	//print_r($response);
    echo json_encode($response);
}


$app->run();
?>
